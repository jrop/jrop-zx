#!/bin/bash

pushd ..
rm *.tgz
npm pack
popd

cp ../*.tgz .
tar -xvf *.tgz
rm -rf node_modules/package/
mv package/ node_modules/

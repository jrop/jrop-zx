#!/bin/bash

pushd zx
bun install
bun tsc
mkdir -p ../build
cp build/*.d.ts ../build/
popd

bun esbuild --bundle --platform=node zx/src/index.ts > zx.dist.js
